class ApplicationMailer < ActionMailer::Base
  default from: "montereydsm@gmail.com"
  layout 'mailer'
end